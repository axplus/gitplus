from setuptools import setup, find_packages


setup(
    name='gitplus',
    version='0.1',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            # 'git-br=gitplus.gitbr:main',
            # 'git-st=gitplus.gitst:main',
            'git-stashcp=gitplus.gitstashcp:main',
            # 'git-co2=gitplus.gitco:main',
        ]
    }
)
