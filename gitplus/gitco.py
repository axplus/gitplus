#!/usr/bin/env python


import subprocess
import os
from argparse import ArgumentParser

# from fnmatch import  fnmatch


class Branch(object):
    def __init__(self, name, current=False):
        self.name = name
        self.current = current


def main():
    parser = ArgumentParser()
    parser.add_argument('glob', nargs='?')

    args = parser.parse_args()

    o = subprocess.check_output('git branch', shell=True)
    branches = []

    for line in o.split('\n'):
        name = line.strip()
        current = False
        if name.startswith('*'):
            current = True
            name = name[1:]  # +'<---------current'
        name = name.strip()
        if not name:
            continue
        branches.append(Branch(name, current))

    if args.glob:
        selec = (br for br in branches if args.glob in br.name)
        print next(selec).name,
    else:
        for br in branches:
            if br.current:
                print '>', br.name
            else:
                print ' ', br.name
