#!/usr/bin/env python


import subprocess
import os
import sys
from argparse import ArgumentParser, REMAINDER

# from fnmatch import  fnmatch


class Branch(object):
    def __init__(self, name, current=False):
        self.name = name
        self.current = current


class Repo(object):
    def __init__(self, repo_path):
        self.repo_path = repo_path
        self.branches = []
        o = subprocess.check_output('git branch', shell=True, cwd=repo_path)

        for line in o.split('\n'):
            name = line.strip()
            current = False
            if name.startswith('*'):
                current = True
                name = name[1:]  # +'<---------current'
            name = name.strip()
            if not name:
                continue
            self.branches.append(Branch(name, current))


class ListBranch(object):
    def __init__(self, argv):
        parser = ArgumentParser()
        self.args = parser.parse_args(argv)

    def execu(self, repo):
        subprocess.call('git branch', shell=True)


class Co(object):
    def __init__(self, argv):
        parser = ArgumentParser()
        parser.add_argument('glob')
        self.args = parser.parse_args(argv)

    def execu(self, repo):
        selec = (br for br in repo.branches if self.args.glob in br.name)
        subprocess.call('git co %s' % next(selec).name, shell=True)


class Reset(object):
    def __init__(self, argv):
        parser = ArgumentParser()
        parser.add_argument('glob')
        self.args = parser.parse_args(argv)

    def execu(self, repo):
        st_output = subprocess.check_output('git st', shell=True)
        modified = []
        for l in st_output.split('\n'):
            if os.path.isfile(l.strip()):
                modified.append(l.strip())
        print modified
        selec = (f for f in modified if self.args.glob in f)
        subprocess.call('git co -- %s' % next(selec), shell=True)


class Help(object):
    CMD_ROUTE_TABLE = {
        'default': 'ListBranch',
        'co': 'Co',
        'reset': 'Reset',  # means co --
        'help': 'Help',
    }

    def __init__(self, argv):
        # parser = ArgumentParser()
        # self.args = parser.parse_args(argv)
        pass

    def execu(self, repo):
        print Help.CMD_ROUTE_TABLE
        # subprocess.call('git branch', shell=True)

    def main(self):

        Cmd_name = Help.CMD_ROUTE_TABLE['default'] if len(
            sys.argv) == 1 else Help.CMD_ROUTE_TABLE[sys.argv[1]]
        Cmd = globals()[Cmd_name]
        repo = Repo('.')
        cmd = Cmd(sys.argv[2:])
        cmd.execu(repo)


def main():
    Help(sys.argv).main()
