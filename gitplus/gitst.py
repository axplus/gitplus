#!/usr/bin/env python


import subprocess
import re


import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--unversioned',
                        action='store_true', help='show unversioned files')

    args = parser.parse_args()

    allfiles = subprocess.check_output('git status', shell=True, cwd='.')
