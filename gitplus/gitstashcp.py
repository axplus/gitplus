#!/usr/bin/env python


import os
import shutil
import glob
import re
import subprocess
import datetime


def main():

    ts = datetime.datetime.now().strftime(r'%Y%m%d%H%M%S')

    # get git modified file
    mpat = re.compile(r'^\s*modified:\s*([^\s]+)')
    p = subprocess.Popen('git st', shell=True, stdout=subprocess.PIPE)
    # modifiedfiles = []
    for line in p.stdout:
        m = mpat.match(line)
        if not m:
            continue

        # load all file name with time, and computer the smallest number
        # copy file with smallest number as version
        ofilepath = m.group(1)
        # files = glob.glob(ofilepath + '.*')
        # files.sort()
        # print files
        # # extract ord from files
        # ord = 99
        # for filepath in files:
        #     print filepath[-20:]
        #     fm = re.search(r'\.(\d+)\.\d+', filepath[-20:])
        #     if fm is None:
        #         continue
        #     ord = min(ord, int(fm.group(1)))

        newpath = '%s.%s' % (ofilepath,  ts)
        print 'backup', m.group(1), 'to', newpath
        shutil.copyfile(m.group(1), newpath)

    # copy the file into modify file
    #
